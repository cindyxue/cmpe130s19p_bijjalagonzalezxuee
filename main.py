#param Edge(id1, id2, distance, steepness)
from node_edge import Node, Edge
from graph import Graph as mars_graph
from plot_map import PlotMap
import math

nodes = [
		Node(0, 189, 545),
		Node(1, 262, 508),
		Node(2, 166, 483),
		Node(3, 245, 460),
		Node(4, 320, 529),
		Node(5, 301, 575),
		Node(6, 359, 608),
		Node(7, 378, 545),
		Node(8, 442, 610),
		Node(9, 471, 543),
		Node(10, 421, 506),
		Node(11, 357, 465),
		Node(12, 309, 427),
		Node(13, 436, 450),
		Node(14, 390, 409),
		Node(15, 283, 371),
		Node(16, 320, 359),
		Node(17, 365, 334),
		Node(18, 256, 311),
		Node(19, 220, 251),
		Node(20, 301, 288),
		Node(21, 347, 253),
		Node(22, 370, 205),
		Node(23, 388, 158),
		Node(24, 322, 158),
		Node(25, 297, 104)
	]

def calc_distance(id1, id2):
    x1 = nodes[id1].get_x()
    y1 = nodes[id1].get_y()
    x2 = nodes[id2].get_x()
    y2 = nodes[id2].get_y()
    dist = math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
    return dist

def plot_shortpath(id1,id2, criterion="distance"):
	g = mars_graph(31)
	g.add_edge(Edge(0, 1, 81, 28, criterion))
	g.add_edge(Edge(0, 2, 66, 29, criterion))
	g.add_edge(Edge(1, 5, 77, 12, criterion))
	g.add_edge(Edge(5, 4, 49, 23, criterion))
	g.add_edge(Edge(2, 3, 82, 90, criterion))
	g.add_edge(Edge(3, 12, 72, 34, criterion))
	g.add_edge(Edge(1, 4, 61, 36, criterion))
	g.add_edge(Edge(4, 7, 60, 45, criterion))
	g.add_edge(Edge(4, 11, 73, 40, criterion))
	g.add_edge(Edge(5, 6, 66, 18, criterion))
	g.add_edge(Edge(7, 6, 65, 68, criterion))
	g.add_edge(Edge(1, 3, 50, 43, criterion))
	g.add_edge(Edge(7, 8, 91, 68, criterion))
	g.add_edge(Edge(8, 9, 73, 46, criterion))
	g.add_edge(Edge(8, 10, 106, 30, criterion))
	g.add_edge(Edge(7, 10, 58, 40, criterion))
	g.add_edge(Edge(7, 9, 93, 28, criterion))
	g.add_edge(Edge(7, 11, 82, 39, criterion))
	g.add_edge(Edge(10, 13, 57, 2, criterion))
	g.add_edge(Edge(11, 12, 61, 25, criterion))
	g.add_edge(Edge(11, 14, 65, 40, criterion))
	g.add_edge(Edge(11, 16, 112, 45, criterion))
	g.add_edge(Edge(12, 13, 129, 67, criterion))
	g.add_edge(Edge(12, 15, 61, 20, criterion))
	g.add_edge(Edge(15, 18, 65, 30, criterion))
	g.add_edge(Edge(16, 17, 51, 34, criterion))
	g.add_edge(Edge(16, 15, 38, 3, criterion))
	g.add_edge(Edge(17, 14, 79, 22, criterion))
	g.add_edge(Edge(17, 20, 78, 35, criterion))
	g.add_edge(Edge(17, 21, 82, 34, criterion))
	g.add_edge(Edge(18, 20, 50, 10, criterion))
	g.add_edge(Edge(18, 19, 69, 15, criterion))
	g.add_edge(Edge(20, 21, 57, 43, criterion))
	g.add_edge(Edge(21, 22, 53, 43, criterion))
	g.add_edge(Edge(21, 24, 98, 34, criterion))
	g.add_edge(Edge(22, 23, 50, 45, criterion))
	g.add_edge(Edge(24, 25, 59, 23, criterion))
	r = g.shortest_path_between(id1, id2)

	path = []

	for p in r:
		path.append((nodes[p[0]],nodes[p[1]]))

	map = PlotMap("mars.png", path)
	map.show_map()

if __name__ == '__main__':
    id1 = input("Enter an integer between (0-25) for your starting point on plot: ")
    id2 = input("Enter an integer between (0-25) for your ending point on plot: ")
    criterion = input("Enter criterion (distance, ave_steepness, work_done, power): ")
    plot_shortpath(int(id1), int(id2), criterion)
