import matplotlib.pyplot as plt
from node_edge import Node, Edge


# marsimg = plt.imread("mars.png")
# fig, ax = plt.subplots()
# ax.imshow(marsimg)

# x, y = [190, 264], [554, 520]
# plt.plot(x, y, marker='o')

# plt.show(block=True)


class PlotMap():
    def __init__(self, map_image, lines):
        """
        PlotMap Constructor
        @var string map_image
        @var [(Node, Node), (Node, Node), ...] lines
        """
        map = plt.imread(map_image)
        fig, ax = plt.subplots()
        for l in lines:
            self.draw_line(l[0], l[1])
        ax.imshow(map)
    
    def draw_line(self, p1, p2):
        """Draw line on the map using two points
        @var Node p1
        @var Node p2
        """
        x, y = [p1.get_x(), p2.get_x()], [p1.get_y(), p2.get_y()]
        plt.plot(x, y, color="g", marker='o')

    def show_map(self):
        plt.show(block=True)