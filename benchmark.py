from node_edge import Node, Edge
from graph import Graph as mars_graph
import time
import random
import matplotlib.pyplot as plt

set_szs = [10, 100, 1000, 10000]
# set_szs = [10, 100, 1000, 10000, 100000, 1000000] if the time allows
timing = []

for set_sz in set_szs:

    graph = mars_graph(set_sz)

    for i in range(set_sz - 1):
        graph.add_edge(Edge(random.randint(0, set_sz-1), random.randint(0, set_sz-1), random.randint(0, 100), random.randint(0, 50)))
    
    t0 = time.time()

    graph.shortest_path_between(random.randint(0, set_sz-1), random.randint(0, set_sz-1))

    t1 = time.time()

    total_time = t1 - t0

    timing.append(total_time)

plt.plot(set_szs, timing)
plt.xscale('log')
plt.yscale('log')
plt.title('Time vs Edges')
plt.ylabel('Time to find shortest paths (s)')
plt.xlabel('Number of edges in graph')
plt.show()
