from math import sin
class Node:
    """Represents a point on Mars' Iani Chaos Region
    """
    def __init__(self, id, x, y):
        """Node constructor
        @var number id
        Node ID
        @var number x
        X-Coordinate
        @var number y
        Y-Coordinate
        """
        self.id = id
        self.x = x
        self.y = y

    def get_x(self):
        return self.x

    
    def get_y(self):
        return self.y

    
    def get_id(self):
        return self.id

class Edge:
    """Represents a path from point to point on Mars' Iani Chaos Region
    """
    def __init__(self, nodeid1, nodeid2, distance, average_steepness, compare="distance"):
        """Edge Constructor
        @var number nodeid1
        ID of the node representing one end of the edge
        @var number nodeid2
        ID of the node representing the other end of the edge
        @var number distance
        Distance to travel from node to node
        @var number average_steepness
        The average steepness of the trail. Angle in degrees
        """

        if not (self._is_positive(nodeid1) or self._is_positive(nodeid2) or self._is_positive(distance) or self._is_positive(average_steepness)):
            raise "Non-negative value required."

        self.nodeid1 = nodeid1
        self.nodeid2 = nodeid2
        self.distance = distance
        self.average_steepness = average_steepness
        self.compare = compare

    def get_either(self):
        """Get id either of the two end nodes.
        """
        return self.nodeid1
    
    def get_other(self,id):
        """Get id of the other end node.
        @var number id
        ID of the node connected to the other node.
        """
        if id == self.nodeid1:
            return self.nodeid2
        return self.nodeid1
    
    def reverse_nodes(self):
        return Edge(self.nodeid1, self.nodeid2, self.distance, self.average_steepness)

    def get_distance(self):
        """Get distance value.
        """
        return self.distance

    def get_average_steepness(self):
        """Get average steepness value.
        """
        return self.average_steepness
    
    def get_power(self):
        """Get the power required for the rover to travel the
        edge.
        """
        w = 2000
        """Weight of the rover in pounds (lbs)"""
        v = 0.03889
        """Velocity of the rover in meters per second"""
        p = w * v * sin(self.average_steepness)
        return abs(p)
    
    def get_work_done(self):
        """Get the work done from travelling the edge.
        """

        w = 2000 # weight of the rover in pounds (lbs)
        v = 0.03889 # velocity of the rover in meters per second
        p = abs(w * v * sin(self.average_steepness)) # calculate power
        t = self.distance / v # calculate the time it would take to travel the distance
        work = p * t # calculate work done

        return work

    def _is_positive(self, x):
        if x < 0:
            return False
        return True
    
    def print_info(self):
        print(self.nodeid1, self.nodeid2, self.distance, self.average_steepness)

    def criterion(self):
        if self.compare == "power":
            return self.get_power()
        elif self.compare == "ave_steepness":
            return self.get_average_steepness()
        elif self.compare == "work_done":
            return self.get_work_done()
        else:
            return self.get_distance()
