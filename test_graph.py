from unittest import TestCase
from node_edge import Node, Edge
from graph import Graph as mars_graph
from plot_map import PlotMap

class TestGraph(TestCase):
    def setup_class(klass):
        """This method is run once for each class before any tests are run"""
        print ("\n#####  Start Function Tests   #####\n")


    def test_one(self):
        pass

    def test_two(self):
        expected = (1, 2, 3)
        actual = (1, 2, 3)
        assert expected == actual

    def test_shortest_path_between(self):
        g = mars_graph(13)
        g.add_edge(Edge(0, 1, 70, 28))
        g.add_edge(Edge(0, 2, 60, 29))
        g.add_edge(Edge(1, 5, 80, 12))
        g.add_edge(Edge(5, 4, 40, 23))
        g.add_edge(Edge(2, 3, 90, 90))
        g.add_edge(Edge(3, 4, 120, 10))
        g.add_edge(Edge(3, 12, 130, 34))
        g.add_edge(Edge(1, 4, 52, 36))
        g.add_edge(Edge(4, 7, 70, 45))
        g.add_edge(Edge(5, 6, 110, 18))
        g.add_edge(Edge(7, 6, 100, 68))
        g.add_edge(Edge(1, 3, 50, 43))

        actual = g.shortest_path_between(3, 6)
        expected = [(5, 6), (1, 5), (3, 1)]

        assert expected == actual

    def test_shortest_path_between2(self):
        g = mars_graph(31)
        g.add_edge(Edge(0, 1, 70, 28))
        g.add_edge(Edge(0, 2, 60, 29))
        g.add_edge(Edge(1, 5, 80, 12))
        g.add_edge(Edge(5, 4, 40, 23))
        g.add_edge(Edge(2, 3, 90, 90))
        g.add_edge(Edge(3, 4, 120, 10))
        g.add_edge(Edge(3, 12, 130, 34))
        g.add_edge(Edge(1, 4, 52, 36))
        g.add_edge(Edge(4, 7, 70, 45))
        g.add_edge(Edge(5, 6, 110, 18))
        g.add_edge(Edge(7, 6, 100, 68))
        g.add_edge(Edge(1, 3, 50, 43))
        g.add_edge(Edge(7, 8, 100, 68))
        g.add_edge(Edge(8, 9, 146, 46))
        g.add_edge(Edge(8, 10, 170, 30))
        g.add_edge(Edge(7, 10, 128, 40))
        g.add_edge(Edge(7, 9, 148, 28))
        g.add_edge(Edge(7, 11, 150, 39))
        g.add_edge(Edge(11, 12, 45, 25))
        g.add_edge(Edge(11, 14, 78, 40))
        g.add_edge(Edge(11, 16, 200, 45))
        g.add_edge(Edge(12, 13, 67, 67))
        g.add_edge(Edge(13, 16, 20, 20))
        g.add_edge(Edge(13, 18, 78, 25))
        g.add_edge(Edge(16, 17, 67, 34))
        g.add_edge(Edge(17, 20, 189, 35))
        g.add_edge(Edge(17, 21, 200, 34))
        g.add_edge(Edge(21, 22, 78, 43))
        g.add_edge(Edge(21, 24, 230, 34))
        g.add_edge(Edge(22, 23, 67, 45))
        g.add_edge(Edge(24, 25, 79, 23))

        actual = g.shortest_path_between(0, 12)
        expected = [(3, 12), (1, 3), (0, 1)]

        assert expected == actual
