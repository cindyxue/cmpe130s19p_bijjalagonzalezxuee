import heapq
from node_edge import Edge

class Graph:
    def __init__(self, nvertices):
        self.nvertices = nvertices
        self.nedges = 0
        self.adjacency_list = [[] for i in range(nvertices)]

        self.edgeTo = []
        self.distTo = []
        self.edges = []
    
    def get_nvertices(self):
        return self.nvertices
    
    def add_edge(self, e):

        v = e.get_either()

        if (v, e.get_other(v)) in self.edges:
            return
        
        self.adjacency_list[v].append(e)

        w = e.get_other(v)
        ne = Edge(w, v, e.criterion(), e.get_average_steepness())
        self.adjacency_list[w].append(ne)
        self.nedges += 1

    def dijkstra_sp(self, start):
        self.edgeTo = [None] * self.nvertices
        self.distTo = [None] * self.nvertices
        self.pq = []
        
        for i in range(0, self.nvertices):
            self.distTo[i] = float("inf")

        self.distTo[start] = 0.0

        heapq.heapify(self.pq)
        heapq.heappush(self.pq, (0.0, start))
        
        while self.pq:
            v = heapq.heappop(self.pq)[1]
            for e in self.adjacency_list[v]:
                self._relax(e)
    
    def _relax(self, e):
        v = e.get_either()
        w = e.get_other(v)
        if (self.distTo[w] > self.distTo[v] + e.criterion()):
            temp = (self.distTo[w], w)
            self.distTo[w] = self.distTo[v] + e.criterion()
            self.edgeTo[w] = e
            if (temp in self.pq):
                self.pq[self.pq.index(temp)] = (self.distTo[w], w)
                heapq.heapify(self.pq)
            else:
                heapq.heappush(self.pq, (self.distTo[w], w))

    def shortest_path_between(self, a, b):
        self.dijkstra_sp(a)
        result = []
        
        if not self.edgeTo[b]:
            return

        edgeTo = self.edgeTo[b].get_other(self.edgeTo[b].get_either())
        while edgeTo != a:
            v = self.edgeTo[edgeTo].get_either()
            w = self.edgeTo[edgeTo].get_other(v)
            result.append((v, w))
            edgeTo = self.edgeTo[edgeTo].get_other(edgeTo)
        
        return result
    
    def show_adjacency_list(self):
        for l in self.adjacency_list:
            for e in l:
                print(e.get_either(), e.get_other(e.get_either()))
    
    def show_edges(self):
        for e in self.edgeTo:
            if e:
                e.print_info()
            else:
                print("")
