**1. Prerequisites:** 
Python3, pip and virtualenv

To install virtualenv:

>$ pip install virtualenv

**2. Create a virtual environment:**

>$ virtualenv venv

Activate the virtual environment on Mac/Linux:
    
>$ source venv/bin/activate

Activate the virtual environment on on Windows:

>$ .\venv\Scripts\activate

**3. Install dependencies:**
>$ pip install -r requirements.txt

**4. Run the program:**
>$ python3 main.py
